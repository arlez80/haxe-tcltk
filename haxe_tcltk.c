#define IMPLEMENT_API
#include <hx/CFFI.h>

#include <tcl.h>
#include <tk.h>

DEFINE_KIND(hx_Tcl_Interp);

extern "C" {
	/* tcl */
	value hx_Tcl_CreateInterp( )
	{
		value v = alloc_abstract( hx_Tcl_Interp, Tcl_CreateInterp( ) );
		val_gc( v, hx_Tcl_Finalize );
		return val_true;
	}

	void hx_Tcl_Finalize( value v )
	{
		hx_Tcl_DeleteInterp( v );
	}

	value hx_Tcl_DeleteInterp( value v )
	{
		val_check_kind( v, hx_Tcl_Interp );
		Tcl_DeleteInterp( val_data( v ) );
		val_kind(v) = NULL;
		return val_true;
	}

	value hx_Tcl_Init( value v )
	{
		if( !val_is_abstract( v ) || !val_is_kind( v, hx_Tcl_Interp ) ) return val_null;
		return Tcl_Init( val_data( v ) ) == TCL_ERROR ? val_false : val_true;
	}

	value hx_Tcl_EvalFile( value v, value a )
	{
		if( !val_is_abstract( v ) || !val_is_kind( v, hx_Tcl_Interp ) ) return val_null;
		if( !val_is_string( a ) ) return val_null;
		return Tcl_EvalFile( val_data( v ), val_string( a ) ) == TCL_ERROR ? val_false : val_true;
	}

	value hx_Tcl_Eval( value v, value a )
	{
		if( !val_is_abstract( v ) || !val_is_kind( v, hx_Tcl_Interp ) ) return val_null;
		if( !val_is_string( a ) ) return val_null;
		return Tcl_Eval( val_data( v ), val_string( a ) ) == TCL_ERROR ? val_false : val_true;
	}

	value hx_Tcl_SetVar( value v, value a, value b )
	{
		if( !val_is_abstract( v ) || !val_is_kind( v, hx_Tcl_Interp ) ) return val_null;
		if( !val_is_string( a ) ) return val_null;
		if( !val_is_string( b ) ) return val_null;
		return Tcl_SetVar( val_data( v ), val_string( a ), val_string( b ), TCL_GLOBAL_ONLY ) == TCL_ERROR ? val_false : val_true;
	}

	value hx_Tcl_GetVar( value v, value a )
	{
		if( !val_is_abstract( v ) || !val_is_kind( v, hx_Tcl_Interp ) ) return val_null;
		if( !val_is_string( a ) ) return val_null;
		return alloc_string( Tcl_GetVar( val_data( v ), val_string( a ), TCL_GLOBAL_ONLY ) );
	}

	int hx_Tcl_CallCommand( ClientData data, Tcl_Interp* interp, int argc, char* argv[] )
	{
		value f = *( (value*) data );
		value a = alloc_array( argc );
		value* ptr = val_array_ptr( a );
		for( int i=0; i<argc; i++ ) {
			ptr[i] = alloc_string( argv[i] );
		}

		value r = val_call1( f, a );
		Tcl_SetResult( interp, val_string( r ), TCL_VOLATILE );
		return TCL_OK;
	}

	value hx_Tcl_CreateCommand( value v, value name, value f )
	{
		if( !val_is_abstract( v ) || !val_is_kind( v, hx_Tcl_Interp ) ) return val_null;
		if( !val_is_string( a ) ) return val_null;
		if( !val_is_function( f ) ) return val_null;

		Tcl_CreateCommand( val_data( v ), val_string( a ), hx_Tcl_CallCommand, (ClientData) &f, NULL );

		return val_true;
	}

	value hx_Tcl_DeleteCommand( value v, value name )
	{
		if( !val_is_abstract( v ) || !val_is_kind( v, hx_Tcl_Interp ) ) return val_null;
		if( !val_is_string( a ) ) return val_null;

		Tcl_DeleteCommand( val_data( v ), val_string( a ) );

		return val_true;
	}

	/* tk */

	value hx_Tk_Init( value v )
	{
		if( !val_is_abstract( v ) || !val_is_kind( v, hx_Tcl_Interp ) ) return val_null;
		return Tk_Init( val_data( v ) ) == TCL_ERROR ? val_false : val_true;
	}

	value hx_Tk_Mainloop( )
	{
		Tk_Mainloop( );
		return val_null;
	}
}

/* generation DEFINE_PRIM */
DEFINE_PRIM( hx_Tcl_CreateInterp, 0 );
DEFINE_PRIM( hx_Tcl_DeleteInterp, 1 );
DEFINE_PRIM( hx_Tcl_Init, 1 );
DEFINE_PRIM( hx_Tcl_EvalFile, 2 );
DEFINE_PRIM( hx_Tcl_Eval, 2 );
DEFINE_PRIM( hx_Tcl_SetVar, 3 );
DEFINE_PRIM( hx_Tcl_GetVar, 2 );
DEFINE_PRIM( hx_Tcl_CreateCommand, 3 );
DEFINE_PRIM( hx_Tcl_DeleteCommand, 2 );
DEFINE_PRIM( hx_Tk_Init, 1 );
DEFINE_PRIM( hx_Tk_Mainloop, 0 );
