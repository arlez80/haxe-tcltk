/**
 * Tcl/Tkのラッパー
 * @author あるる（きのもと 結衣）
 */

package tcltk;

typedef TclCommandFunc = Array<String>->Void;
typedef TclInterp = {};

class Tcl
{
#if cpp
	static var CreateInterp:Void->Void = cpp.Lib.load( "tcltk", "hx_Tcl_CreateInterp", 0 );
	static var DeleteInterp:TclInterp->Void = cpp.Lib.load( "tcltk", "hx_Tcl_DeleteInterp", 1 );
	static var Init:TclInterp->Void = cpp.Lib.load( "tcltk", "hx_Tcl_Init", 1 );
	static var EvalFile:TclInterp->String->Void = cpp.Lib.load( "tcltk", "hx_Tcl_EvalFile", 2 );
	static var Eval:TclInterp->String->Void = cpp.Lib.load( "tcltk", "hx_Tcl_Eval", 2 );
	static var SetVar:TclInterp->String->String->Void = cpp.Lib.load( "tcltk", "hx_Tcl_SetVar", 3 );
	static var GetVar:TclInterp->String->Void = cpp.Lib.load( "tcltk", "hx_Tcl_SetVar", 2 );
	static var CreateCommand:TclInterp->String->TclCommandFunc->Void = cpp.Lib.load( "tcltk", "hx_Tcl_SetVar", 3 );
	static var DeleteCommand:TclInterp->String->Void = cpp.Lib.load( "tcltk", "hx_Tcl_SetVar", 2 );
#elseif neko
	static var CreateInterp:Void->Void = neko.Lib.load( "tcltk", "hx_Tcl_CreateInterp", 0 );
	static var DeleteInterp:TclInterp->Void = neko.Lib.load( "tcltk", "hx_Tcl_DeleteInterp", 1 );
	static var Init:TclInterp->Void = neko.Lib.load( "tcltk", "hx_Tcl_Init", 1 );
	static var EvalFile:TclInterp->String->Void = neko.Lib.load( "tcltk", "hx_Tcl_EvalFile", 2 );
	static var Eval:TclInterp->String->Void = neko.Lib.load( "tcltk", "hx_Tcl_Eval", 2 );
	static var SetVar:TclInterp->String->String->Void = neko.Lib.load( "tcltk", "hx_Tcl_SetVar", 3 );
	static var GetVar:TclInterp->String->Void = neko.Lib.load( "tcltk", "hx_Tcl_SetVar", 2 );
	static var CreateCommand:TclInterp->String->TclCommandFunc->Void = neko.Lib.load( "tcltk", "hx_Tcl_SetVar", 3 );
	static var DeleteCommand:TclInterp->String->Void = neko.Lib.load( "tcltk", "hx_Tcl_SetVar", 2 );
#end
}

class Tk
{
#if cpp
	static var Init:TclInterp->Void = cpp.Lib.load( "tcltk", "hx_Tk_Init", 1 );
	static var Mainloop:Void->Void = cpp.Lib.load( "tcltk", "hx_Tk_Mainloop", 0 );
#elseif neko
	static var Init:TclInterp->Void = neko.Lib.load( "tcltk", "hx_Tk_Init", 1 );
	static var Mainloop:Void->Void = neko.Lib.load( "tcltk", "hx_Tk_Mainloop", 0 );
#end
}
